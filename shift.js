function shift(array,shiftType,shiftNumber){
    lengthArray = array.length
    if(shiftType === 'left'){
        tempArray = []
        array.map((value,index) => {
            indexArray = (index - shiftNumber) % lengthArray
            indexShift = indexArray  >= 0 ? 
                        indexArray  : 
                        indexArray + lengthArray  
            tempArray[indexShift] = value
        }) 
        return tempArray
    }else
    if(shiftType === 'right'){
        tempArray = []
        array.map((value,index) => {
            indexShift = (index + shiftNumber) % lengthArray
            tempArray[indexShift] = value
        }) 
        return tempArray
    }else {
        return 'use left or right only'
    }
}
console.log(shift([1,2,3,4,5,6],'right',7))
