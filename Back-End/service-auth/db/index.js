const mongoose = require('mongoose')

function init(){
    return new Promise((reslove,reject) => {
        mongoose.connect('mongodb://mongo:27017', { dbName: 'test', useNewUrlParser: true, useUnifiedTopology: true, user: 'admin', pass: 'password' }, err => {
            if (err) { 
                return reject(err) 
            }
            console.log("database mongo connect");
            return reslove("database mongo connect")
        })
    })
}
// const { User } = require('../model/user')

// const a = new User({ 
//     "_id": "user12", 
//     "password": "1bfjdsvnno20g", 
//     'firstName': "firstName", 
//     "lastName": "lastName" })
//     .save()
//     .then((respon) => {
//         console.log(respon);
//     })
module.exports = {
    init
}