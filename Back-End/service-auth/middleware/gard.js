const jwt = require('jsonwebtoken');
const { jwtConfig } = require('../config');
const moment = require('moment')

module.exports = function (req, res, next) {
    let authorizationHeader = req.headers['authorization'];
    let accessToken;

    if (authorizationHeader) {
        authorizationHeader = authorizationHeader.split(' ');
    } else {
        return res.sendStatus(401).send(
            {
                // code:'401', // ไม่ใส่ auth header
                result:false
            }
        );;
    }

    if (authorizationHeader[0] === 'Bearer' && authorizationHeader[1]) {
        accessToken = authorizationHeader[1];
    }

    if (accessToken) {
        jwt.verify(
            accessToken,
            jwtConfig.accessTokenPublicKEY,
            { algorithms: ['RS256'] },
            function (err, decoded) {
                if (err) {
                    res.sendStatus(401);
                } else {
                    if (decoded.exp >= moment().format('X')) {
                        req.user = decoded.userInfo;
                        req.token = accessToken;
                        next();
                    } else {
                        res.sendStatus(401).send(
                            {
                                // code:'410',
                                result:false,
                                message:'token expire'
                            }
                        );
                    }
                }
            }
        );
    } else {
        res.sendStatus(401).send(
            {
                // code:'400',
                result:false
            }
        );
    }
};
