const db = require('../db')
const { jwtConfig } = require('../config');
const { User } = require('../model/user')
const jwt = require('jsonwebtoken');
async function Login(req, res) {
    try {
        
        if (req.headers.authorization) {
            hearders = req.headers.authorization.split(' ')
            buff = new Buffer.from(hearders[1], 'base64');
            let text = buff.toString('utf-8').split(':')
            username = text[0]
            password = require('crypto').createHash('sha256').update(text[1]).digest('hex')

        } else {
            res.set('WWW-Authenticate', 'Basic realm="401"')
            return res.status(401).send({
                result: false,
                code: 401,
                message: "Login Use basic auth"
            })
        }
        var data  = await User.findOne({_id:username,password:password}).then(user => {
            return user
        });
        console.log(data);
        if(data){
            signPayload = {
                userInfo: {
                    ...data[0]
                }
            };
            const accessToken = jwt.sign(
                signPayload,
                jwtConfig.accessTokenPrivateKEY,
                {
                    algorithm: 'RS256',
                    expiresIn: jwtConfig.accessTokenExpiresIn
                }
            );
            return res.status(200).send({
                result: true,
                token: accessToken,
                payload:data[0]
            })
        }else{
            return res.status(401).send({
                result: false,
                message: 'username or password incorract'
            })
        }
        
    } catch (err) {

        res.status(500).send({
            result: false,
            // code: 500,
            message: err.message
        })
    }
}

module.exports = {
    Login
}