const db = require('../db')
const { User } = require('../model/user')
async function Register(req, res) {
    try {
        var {username ,password,firstName,lastName} = req.body
        new User(
            {
                _id:username,
                password:require('crypto').createHash('sha256').update(password).digest('hex'),
                firstName:firstName,
                lastName:lastName
            }
        ).save(function(err,doc) {
            if(err){
                return res.status(500).send({
                    result: false,
                    message: 'user is register in Storage'
                })
            }else{
                return res.status(200).send({
                    result: true,
                    payload: doc
                })
            }
        })
        // console.log(data);
        
        
    } catch (err) {

        res.status(500).send({
            result: false,
            message: err.message
        })
    }
}

module.exports = {
    Register
}