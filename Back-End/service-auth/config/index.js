const fs = require('fs')

module.exports = {
  "mysqlConfig": {
    // connectionLimit: 200, // Just to curb our enthusiasm.
    host: process.env.MYSQL_HOST || '35.213.137.81',
    user: process.env.MYSQL_USER || 'root',
    password: process.env.MYSQL_PASS || 'dos@database',
    port: process.env.MYSQL_PORT || 3306,
    database: process.env.MYSQL_DATABASE_NAME || 'seversitup',
    multipleStatements: true,
    insecureAuth : true
  },
  jwtConfig: {
    accessTokenExpiresIn: '30d',
    accessTokenPrivateKEY:
      fs.readFileSync('./secret/accessToken.key', 'utf8'),
    accessTokenPublicKEY:
      fs.readFileSync('./secret/accessToken.key.pub', 'utf8')
  }

}
