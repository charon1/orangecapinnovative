const mongoose = require('mongoose');
const { Int } = require('mssql');
const Schema = mongoose.Schema;


const JokeSchema = new Schema({
    header: {
        type: String,
        required: true
    },
    body:{
        type: String,
        required: true
    },
    like:{
        type: Number,
        required:true,
        default: 0
    },
    dislike:{
        type: Number,
        required:true,
        default: 0
    },
    createTime:{
        type: Date,
        required:true,
        default: new Date()
    },
})

module.exports = {
    JokeSchema: mongoose.model('joke', JokeSchema)
}
