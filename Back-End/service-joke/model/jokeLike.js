const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const JokeLikeSchema = new Schema({
    user: {
        type: String,
        required: true
    },
    jokeId:{
        type: String,
        required: true
    },
    like:{
        type: Boolean
    },
    creaetTime:{
        type: Date,
        default: new Date()
    }
})

module.exports = {
    JokeLikeSchema: mongoose.model('jokeLike', JokeLikeSchema)
}
