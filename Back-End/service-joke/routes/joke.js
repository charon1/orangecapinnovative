const Router = require('express-promise-router');
const gard = require('../middleware/gard');
const router = new Router();
const { AddJoke } = require('../controller/AddJoke')
const { GetAllJoke } = require('../controller/GetAllJoke')
const { GetJokeId } = require('../controller/GetJokeId')
const { LikeJoke } = require('../controller/LikeJoke')
const { DislikeJoke } = require('../controller/DislikeJoke')
const { DeleteJoke } = require('../controller/DeleteJoke')
// const { Logout } = require('../controller/Logout')
// const { VerifyToken } = require('../controller/VerifyToken')
// const gard = require('../middleware/gard')
// /* GET users listing. */
router.post('/addJoke',gard, AddJoke);
router.get('/get', GetAllJoke);
router.get('/getId', GetJokeId);
router.delete('/delete',gard, DeleteJoke);
router.get('/like', LikeJoke);
router.get('/dislike', DislikeJoke);

// router.get('/logout',gard, Logout);
// router.get('/verify',gard, VerifyToken);

module.exports = router;
