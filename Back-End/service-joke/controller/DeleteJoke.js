const db = require('../db')
const { JokeSchema } = require('../model/joke')
async function DeleteJoke(req, res) {
    try {
        await JokeSchema.deleteOne({_id:req.query.id}).then(joke => {
            if(joke.deletedCount === 0) {
                return res.status(400).send({
                    result: false,
                    payload: 'Not have data delete',
                })
            }else{
                return res.status(200).send({
                    result: true,
                    payload: joke,
                })
            }
        });
        
        
    } catch (err) {
        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    DeleteJoke
}