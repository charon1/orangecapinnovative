const e = require('express');
const db = require('../db')
const { JokeSchema } = require('../model/joke')
async function GetJokeId(req, res) {
    try {
        var data  = await JokeSchema.findOne({_id:req.query.id}).then(joke => {
            if(joke){
                return res.status(200).send({
                    result: true,
                    payload: data,
                })
            }else{
                return res.status(200).send({
                    result: false,
                    payload: data,
                    message:'Not have id in Storeage'
                })
            }
        });
        
        
    } catch (err) {
        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    GetJokeId
}