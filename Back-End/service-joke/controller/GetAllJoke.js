const db = require('../db')
const { JokeSchema } = require('../model/joke')
async function GetAllJoke(req, res) {
    try {
        
        var data  = await JokeSchema.find({}).then(joke => {
            return joke
        });
        return res.status(200).send({
            result: true,
            payload: data,
        })
        
    } catch (err) {
        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    GetAllJoke
}