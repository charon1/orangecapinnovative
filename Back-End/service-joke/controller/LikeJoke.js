const db = require('../db')
// const { JokeLikeSchema } = require('../model/jokeLike')
const { JokeSchema } = require('../model/joke')
async function LikeJoke(req, res) {
    // var user = req.user._id
    var id = req.query.id
    console.log(id);
    try {
        if ( !id) {
            return res.status(400).send({
                result: false,
                message: 'Don t Have id or like in query'
            })
        }
        // var data = 
        await JokeSchema.findById(id,function (err, doc) {
            if (err) { 
                return res.status(500).send({
                result: false,
                message: err.message
            })
            }
            doc.like = doc.like + 1;
            doc.save();
          })
        //   .then(
        //     (value) => {
        //         // console.log(value);
        //         value.like = value.like + 1;
        //         return value
        //     } )
        
        return res.status(200).send({
            result: true,
            message:"like success"
            // payload: await data,
        })

    } catch (err) {
        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    LikeJoke
}