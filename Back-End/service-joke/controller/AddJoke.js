const db = require('../db');
const joke = require('../model/joke');
const { JokeSchema } = require('../model/joke')
async function AddJoke(req, res) {
    try {
        var {header ,body} = req.body
        var data  = new JokeSchema(
            {
                header:header,
                body:body
            }
        ).save().then(joke => {
            return joke
        });
        return res.status(200).send({
            result: true,
            payload:await data
        })
        
    } catch (err) {

        res.status(500).send({
            result: false,
            message: err.message
        })
    }
}

module.exports = {
    AddJoke
}