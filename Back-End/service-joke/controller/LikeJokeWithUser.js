const db = require('../db')
const { JokeLikeSchema } = require('../model/jokeLike')
const { JokeSchema } = require('../model/joke')
async function LikeJoke(req, res) {
    var user = req.user._id
    var id = req.query.id
    var like = req.query.like

    try {
        if (typeof (like) !== 'boolean' || !id) {
            return res.status(400).send({
                result: false,
                message: 'Don t Have id or like in query'
            })
        }
        await JokeSchema.findOne({ _id: id }).then(
            (value) => {
                if (!value) {
                    return res.status(400).send({
                        result: false,
                        message: 'Not have ID in Storege'
                    })
                }
                return value
            }
        )
        await JokeLikeSchema.findOneAndUpdate({ user: user },{like:true,dislike:false}).then(data => {
            if(!data){
                new JokeLikeSchema({
                    user: req.user._id,
                    jokeId: id,
                    like: like
                }).save().then(joke => {
                    return joke
                });
            }
            return data
        })

        
        var jokeLike = await JokeLikeSchema.find({ _id: id }).count
        var jokeDislike = await JokeLikeSchema.find({ _id: id }).count
        await JokeSchema.findOneAndUpdate({ _id: id }, { like: jokeLike, dislike: jokeDislike }).then(
            (value) => {
                if (!value) {
                    return res.status(400).send({
                        result: false,
                        message: 'Not have ID in Storege'
                    })
                }
                return value
            }
        )
        
        return res.status(200).send({
            result: true,
            payload: await data,
        })

    } catch (err) {
        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    LikeJoke
}